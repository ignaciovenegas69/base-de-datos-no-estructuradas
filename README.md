# Operaciones de consulta en MongoDB

Aquí se muestran algunas operaciones comunes de consulta que puedes realizar en MongoDB utilizando el shell de MongoDB.

## Insertar Documentos
- `db.<colección>.insertMany`:→ Permite insertar varios documentos en una colección.

## Consultar Documentos
- `db.<colección>.find()`:→ Se puede revisar el contenido de una colección con este método.
- `db.<colección>.find().pretty()`:→ Para mostrar el contenido de la colección de forma más ordenada.



## Operadores de comparación

### Igualdad
- `db.inventario.find( { cantidad: { $eq: 20 } } )`:→ Coincide con valores que son iguales a un valor especificado.
- `db.inventario.find( { "articulo.nombre": { $eq: "ab" } } )`:→ Busca documentos con el nombre del artículo igual a "ab".

### Mayor que 
- `db.inventario.find( { cantidad: { $gt: 20 } } )`:→ Coincide con valores que son mayores que un valor especificado
- `db.inventario.find( { "transportista.tarifa": { $gt: 3 } } )`:→ Selecciona todos los documentos de la colección inventario donde la tarifa (campo de un documento anidado) es mayor a 3.

### Mayor o igual que 
- `db.inventario.find( { cantidad: { $gte: 30 } } )`:→ Coincide con valores que son mayores o iguales a un valor especificado.
- `db.inventario.find( { “transportista.tarifa”: { $gte: 3 } } )`:→ Para seleccionar todos los documentos de la colección inventario donde la tarifa (campo de un documento anidado)

### Coincide con cualquier valore especificado
- `db.inventario.find( { cantidad: { $in: [ 5, 15 ] } }, { _id: 0 } )`:→ Coincide con cualquiera de los valores especificados en un arreglo

### Menor que  
- `db.inventario.find( { cantidad: { $lt: 50 } } )`:→ Coincide con valores que son menores que un valor especificado
- `db.inventario.find( { "transportista.tarifa": { $lt: 5 } } )`:→ Selecciona todos los documentos de la colección inventario donde la tarifa (campo de un documento anidado) es menor a 5.

### Menor o igual que
- `db.inventario.find( { cantidad: { $lte: 30 } } )`:→ Coincide con valores que son menores o iguales a un valor especificado
- `db.inventario.find( { "transportista.tarifa": { $lte: 3 } } )`:→ Selecciona todos los documentos de la colección inventario donde la tarifa (campo de un documento anidado) es menor o igual a 3.

### Coincide con todos los valores que NO son iguales a un valor especificado.
- `db.inventario.find( { cantidad: { $ne: 20 } } )`:→ Coincide con todos los valores que no son iguales a un valor especificado.
- `db.inventario.find( { "transportista.tarifa": { $ne: 3 } } )`:→ Selecciona todos los documentos de la colección inventario donde la tarifa (campo de un documento anidado) es distinta de 3.

### NO coincide con cualquier valore especificado
- `db.inventario.find( { etiquetas: { $nin: [ "escuela", "casa" ] } } )`:→ No coincide con ninguno de los valores especificados en un arreglo

![alt tag](Captura de pantalla 2024-06-25 180254.png)


## Operadores lógicos

### $exists
- `db.inventario.find( { cantidad: { $exists: true, $nin: [ 5, 15 ] } } )`	=→ Esta consulta seleccionará todos los documentos de la colección inventario donde existe el campo cantidad y su valor no es igual 5 o 15.
### AND
- `db.inventario.find( { $and: [{ precio: { $ne: 1.99 } }, { precio: { $exists: true } } ] } )`:→ Une las cláusulas de consulta con un AND lógico, retornando todos los documentos que coinciden con las condiciones de ambas cláusulas.

### NOT
- `db.inventario.find( { precio: { $not: { $gt: 1.99 } } } )`:→ Invierte el efecto de una expresión de consulta y devuelve documentos que no coinciden con la expresión de consulta.

### NOR
- `db.inventario.find( { $nor: [ { precio: 1.99 }, { cantidad: { $lt: 20 } }, { venta: true } ] } )`:→ Une las cláusulas de consulta con un NOR lógico, retornando todos los documentos que no coinciden con ambas cláusulas.

### OR
- `db.inventario.find( { $or: [ { cantidad: { $lt: 20 } }, { precio: 10 } ] } )`:→ Une las cláusulas de consulta con un OR lógico, retornando todos los documentos que coinciden con las condiciones de cualquiera de las cláusulas.

### Combinación de operadores
- `db.inventario.find( { $and: [ { $or: [ { cantidad: { $lt: 10 } }, { cantidad : { $gt: 50 } } ] }, { $or: [ { venta: true }, { precio : { $lt: 5 } } ] } ] } )`:→ Encuentra documentos que cumplen ciertas condiciones combinando operadores lógicos.

##  operadores $type 
![alt tag](Captura de pantalla 2024-05-09 035910.png)

### EJEMPLOS
- `db. libroDirecciones.find( { "CP" : { $type: 1 } } );`
- `db.grados.find( { “promedio" : { $type: [ 2 , 1 ] } } );`
- `db. lecturaSensores.find( { “lecturas" : { $type: "array" } } )`
- `db. libroDirecciones.find( { "CP" : { $type : 2 } } );`

##  operadores $regex 
![alt tag](Captura de pantalla 2024-05-09 041126.png)

### El patrón ^ indica inicio de línea


### EJEMPLOS
- `db.productos.find( { sku: { $regex: /789$/ } } )`
- `db.productos.find( { sku: { $regex: /^ABC/i} } )`
 

# CRUD
## Métodos para Eliminar Documentos

- `db.collection.deleteOne({filtro}) →`: → Elimina el primer documento de la colección, o el primer documento que calce con el filtro indicado
- `db.collection.deleteMany({filtro}) → `:→ Elimina todos los documentos que calzan con el filtro indicado

### Ejemplo
- ` db.peliculas.deleteMany({productor:"Rick McCallum"}) `
- `db.peliculas. deleteOne({escritor:"George Lucas"}) `

## Métodos para Actualizar Documentos

- `db.collection.updateOne({filtro}, {modificación}) →`:→Actualiza el primer documento de la colección, o el primer documento que calce con el filtro indicado.

- `db.collection.updateMany({filtro}, {modificación}) →`:→Actualiza todos los documentos que calzan con el filtro indicado. 

- `db.collection.replaceOne({filtro}, {modificación}) → `:Reemplaza un único documento completo, según el filtro indicado. 

### Ejemplo

- `db.inventario.updateMany({_id:{$lte:5}},{$set:{"transportistas.nombre"."trans valpo" , "transportista.tarifa":50}}) `

## Otros Operadores de Actualización


![alt tag](Captura de pantalla 2024-05-28 165215.png)

### Los mas utilizados:

- `$min`:→Actualiza el valor de un campo, sólo si el valor indicado en la instrucción es menor al valor del campo. Si el campo no existe lo crea con el valor indicado en la instrucción

- `$max`:→Actualiza el valor de un campo, sólo si el valor indicado en la instrucción es mayor al valor del campo. Si el campo no existe lo crea con el valor indicado en la instrucción

- `$setOnInsert `:→se usa para definir qué campos se deben agregar a un documento solo durante una inserción (upsert), no durante una actualización regular.

### Ejemplo

- `db.productos.updateMany({}, { $min: { precio_minimo: 1800 } }, { upsert: true });`

- `db.productos.updateMany({}, { $max: { precio_maximo: 2400 } }, { upsert: true });`

- `db.productos.updateMany({ articulo: 'tornillos' }, { $setOnInsert: { precio_venta: 2000 } }, { upsert: true });`

# Documentos con matrices en MongoDB

- `$size`:→ entrega los documentos donde una matriz tenga una cierta cantidad de elementos
- `{ <campo matriz> : { $size: integer } }`

- `$all` :→ entrega los documentos donde en una matriz podamos buscar algunos elementos que tenga la matriz
- `{ <campo matriz> : { $all: [ valor1,…, valorN ] } }`

- `$elemMatch`:→ entrega los documentos buscando en una matriz de objetos consultando por elementos internos
- `{ <campo matriz> : { $elemMatch: { <operador1>,… <operadorN> } } }` 

- `$slice`:→ entrega documentos cortando elementos de la matriz, indicando cuantos elementos queremos mostrar.
- `{<filtro>}, { <campo matriz> : { $slice: integer o [saltos, cantidad] } }`

### Ejemplos

- `db.personas.find({ hobbies: { $size: 2 } })`

- `db.personas.find({hobbies: { $all: ['jugar', 'leer'] } }) `

- `db.personas.find( {bonos: { $elemMatch: {$gt:30, $lt:55} } } ) `

- `db.personas.find( { hobbies: { $exists: true} } , { hobbies: { $slice: 2} , _id: 0 } ) `

# Documentos incrustados en MongoDB

Cuando se tienen documentos incrustados se utiliza la notación de punto para acceder a los detalles del documento mas interno. Cuando ocurre esto, el campo debe estar en comillas simples o dobles.

- `db.coleccion.find({'acceso.nivel' : 5}) ` Por ejemplo si se quisiera filtrar por nivel

También puede ser de la forma

- `db.coleccion.find({acceso:{nivel : 5}})  `

![alt tag](Captura de pantalla 2024-06-25 173528.png)

- `A esto generalmente se le conoce como modelo desnormalizado `
# Podemos especificar solamente mostrar una columna de matrices (PROYECCIONES)
se tiene la siguiente base de datos y se desea consultar por el color "rojo"
- `db. inventory.find ( {tags:'rojo' }, { item:1, tags:1, _id:0 } )`
al hacerlo de la forma anterior se aroja lo siguiente 

![alt tag](Captura de pantalla 2024-06-25 174808.png)

Pero al hacerlo con la notacion " $ " se obtiene de forma mas ordenada. 

- `db. inventory.find ( {tags:'rojo' }, { item:1, 'tags.$':1, _id:0 } ) `

![alt tag](Captura de pantalla 2024-06-25 174822.png)

- `Explicacion:`:→  $ en 'tags.$' se utiliza en la proyección para referirse al primer elemento del array tags que coincide con el filtro de búsqueda {tags: 'rojo'}. Esto asegura que, si tags es un array y contiene múltiples valores, solo se devolverá el primer elemento que sea 'rojo'.

# Documentos incrustados en MongoDB
## Actualización de valores en Matrices de Documentos incrustados

- `$` 
- → El operador $push agrega un valor especificado a una matriz. Si el campo está ausente en el documento a actualizar, $push agrega el campo de matriz con el valor como su elemento, pero si el campo no es una matriz, la operación fallará.

![alt tag](Captura de pantalla 2024-07-02 143111.png)

- → Pero Podemos usar el operador $push con el modificador $each para agregar varios valores al campo de matriz.

![alt tag](Captura de pantalla 2024-07-02 143319.png)

### En resumen se Puede utilizar el operador $push con los siguientes modificadores:


![alt tag](Captura de pantalla 2024-07-02 143434.png)


## $pull
El operador $pull elimina de una matriz existente todas las instancias de un valor o valores que coincidan con una condición específica. 
### Ejemplo 1 $pull
- `Crearemos otro ejemplo para eliminar todos los elementos que coincidan con una condición específica`

Creamos la colección perfiles
`db.perfiles.insertOne( { _id: 1, votos: [ 3, 5, 6, 7, 7, 8 ] } )`

La siguiente operación eliminará todos los elementos de la matriz votos que sean mayores o iguales que 6:

`db.perfiles.updateOne( { _id: 1 }, { $pull: { votos: { $gte: 6 } } } )`

Después de la operación de actualización, el documento solo tiene valores inferiores a 6:

`{ _id: 1, votes: [ 3, 5 ] }`

### Ejemplo 2 $pull

Crearemos otro ejemplo para eliminar todos los elementos que coincidan con una condición específica, para varios documentos

`db.encuesta.insertMany([ { _id: 1, resultado: [ { item: "A", pts: 5 }, { item: "B", pts: 8 } ] }, { _id: 2, resultado: [ { item: "C", pts: 8 }, { item: "B", pts: 4 } ] } ] )`

La siguiente operación elimina todos los elementos de la matriz resultados que contienen un campo pts igual a 8 y un campo item igual a "B":

`db.encuesta.updateMany( { },{ $pull: { resultado: { pts: 8 , item: "B" } } } )`

## $pop

El operador $pop elimina el primer o último elemento de una matriz. 

Debe pasar un valor $pop de -1 para eliminar el primer elemento de una matriz y un valor $pop de 1 para eliminar el último elemento de una matriz

La operación $pop falla si el <campo> con el que se trabaja no es una matriz.

Si el operador $pop elimina el último elemento del <campo>, entonces el <campo> mantendrá una matriz vacía.

### Ejemplo 1 $pop

![alt tag](Captura de pantalla 2024-07-02 151653.png)


