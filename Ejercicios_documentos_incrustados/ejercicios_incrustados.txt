1. Modificar el precio del juego fifa 2021 para la Xbox a 50000

R:
db.juego.updateOne(
  { 
    nombre: 'fifa 2022', 
    'info.costos.consola': 'xbox' 
  },
  { 
    $set: { 'info.costos.$.valor': 50000 } 
  }
);



2. Reducir el precio del juego sims 4 para el ps5 en 15000

R:
db.juego.updateOne(
  {
    nombre: 'sims 4',
    'info.costos.consola': 'ps5'
  },
  {
    $inc: { 'info.costos.$.valor': -15000 }
  }
);


3. Agregar un campo llamado calificación y agregar una estrella (*)

R:
db.juego.updateMany(
    {},
    {$push:{'calificación':'*'} } 
    )
